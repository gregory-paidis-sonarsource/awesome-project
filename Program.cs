﻿
using System.Collections;

var possiblySet = new List<string>
{
"CHANGE_TARGET",                        // Jenkins
"GITHUB_BASE_REF",                      // Github Actions
"BITBUCKET_PR_DESTINATION_BRANCH",      // Bitbucket Pipelines
"System.PullRequest.TargetBranch",      // Azure DevOps
"CI_MERGE_REQUEST_TARGET_BRANCH_NAME",  // GitLab CI
"TRAVIS_BRANCH",                        // Travis CI
"CM_PULL_REQUEST_DEST",                 // CodeMagic
};

Console.WriteLine("============================================================================");
Console.WriteLine("Environment Variables dump");
Console.WriteLine("============================================================================");
foreach (DictionaryEntry kv in Environment.GetEnvironmentVariables())
{
    Console.WriteLine("{0, -30} {1, 40}", kv.Key, kv.Value);
}

Console.WriteLine("============================================================================");
Console.WriteLine("Base branch variables");
Console.WriteLine("============================================================================");
foreach (var key in possiblySet)
{
    var value = Environment.GetEnvironmentVariable(key);
    Console.WriteLine("{0, -30} {1, 40}", key, value);
}
